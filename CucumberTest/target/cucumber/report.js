$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("cucumberJava/LoginFunctionality.feature");
formatter.feature({
  "line": 1,
  "name": "LoginFunctionality",
  "description": "",
  "id": "loginfunctionality",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Login account e-mail recovery",
  "description": "",
  "id": "loginfunctionality;login-account-e-mail-recovery",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I open web browser",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I open Sportdirect website",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I close popup dialog",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click SignIn button",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I click on email recovery link",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I fill in e-mail address",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I press SendEmail button",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "Recovery confirmation message appears",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "Browser is closed",
  "keyword": "And "
});
formatter.match({
  "location": "stepBindings.openBrowser()"
});
formatter.result({
  "duration": 5006903012,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.openWeb()"
});
formatter.result({
  "duration": 7944442561,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.closeDialog()"
});
formatter.result({
  "duration": 4271363904,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.signIn()"
});
formatter.result({
  "duration": 4082347051,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.recoveryOpen()"
});
formatter.result({
  "duration": 4102638843,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.fillEmailAddress()"
});
formatter.result({
  "duration": 137372117,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.sendEmail()"
});
formatter.result({
  "duration": 4094324349,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.recoveryConfirmation()"
});
formatter.result({
  "duration": 24698046,
  "error_message": "org.openqa.selenium.NoSuchElementException: Unable to locate element: [id$\u003d_ctl00_lblMessage]\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.4.0\u0027, revision: \u0027unknown\u0027, time: \u0027unknown\u0027\nSystem info: host: \u0027CR320248\u0027, ip: \u0027169.254.98.206\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027x86\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_144\u0027\nDriver info: org.openqa.selenium.firefox.FirefoxDriver\nCapabilities [{moz:profile\u003dC:\\Users\\slaati\\AppData\\Local\\Temp\\rust_mozprofile.I5fzLwdhjZNl, rotatable\u003dfalse, timeouts\u003d{implicit\u003d0.0, page load\u003d300000.0, script\u003d30000.0}, pageLoadStrategy\u003dnormal, platform\u003dANY, specificationLevel\u003d0.0, moz:accessibilityChecks\u003dfalse, acceptInsecureCerts\u003dfalse, browserVersion\u003d52.2.1, platformVersion\u003d10.0, moz:processID\u003d7004.0, browserName\u003dfirefox, javascriptEnabled\u003dtrue, platformName\u003dwindows_nt}]\nSession ID: 5c022e67-7488-405d-8a5a-1fb738e3d6d2\n*** Element info: {Using\u003dcss selector, value\u003d[id$\u003d_ctl00_lblMessage]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:150)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:115)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:45)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:82)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:637)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:410)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByCssSelector(RemoteWebDriver.java:501)\r\n\tat org.openqa.selenium.By$ByCssSelector.findElement(By.java:430)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:402)\r\n\tat cucumberJava.stepBindings.recoveryConfirmation(stepBindings.java:184)\r\n\tat ✽.Then Recovery confirmation message appears(cucumberJava/LoginFunctionality.feature:19)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "stepBindings.closeBrowser()"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("cucumberJava/SearchFiltering.feature");
formatter.feature({
  "line": 1,
  "name": "SearchFiltering",
  "description": "",
  "id": "searchfiltering",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Item filter functionality works",
  "description": "",
  "id": "searchfiltering;item-filter-functionality-works",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I open web browser",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "I open Sportdirect website",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I close popup dialog",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I open Mens menu",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I click on Mens shoes link",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I select Firetrap and Skechers brands",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I select the prices between 30 and 60 EUR\u0027s",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I press button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I validate that I\u0027ve selected only Firetrap and Skechers brands",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "I validate that prices are between 30 and 60 EUR\u0027s",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "Browser is closed",
  "keyword": "And "
});
formatter.match({
  "location": "stepBindings.openBrowser()"
});
formatter.result({
  "duration": 3934098530,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.openWeb()"
});
formatter.result({
  "duration": 9718197907,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.closeDialog()"
});
formatter.result({
  "duration": 4196577188,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.openMenu()"
});
formatter.result({
  "duration": 66863581,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.openSubMenu()"
});
formatter.result({
  "duration": 8294144422,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.selectItems()"
});
formatter.result({
  "duration": 4283102905,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.selectPrice()"
});
formatter.result({
  "duration": 4143865919,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.pressButton()"
});
formatter.result({
  "duration": 156451453,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.sortItem()"
});
formatter.result({
  "duration": 7431926640,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.sortPrice()"
});
formatter.result({
  "duration": 4337687078,
  "status": "passed"
});
formatter.match({
  "location": "stepBindings.closeBrowser()"
});
formatter.result({
  "duration": 497471558,
  "status": "passed"
});
});