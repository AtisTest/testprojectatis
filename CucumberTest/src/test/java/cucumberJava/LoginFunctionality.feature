Feature: LoginFunctionality

Scenario: Login account e-mail recovery

	Given I open web browser

	When I open Sportdirect website

		And I close popup dialog

		And I click SignIn button

		And I click on email recovery link

		And I fill in e-mail address

		And I press SendEmail button

	Then Recovery confirmation message appears

		And Browser is closed