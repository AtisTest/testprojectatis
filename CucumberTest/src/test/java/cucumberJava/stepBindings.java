package cucumberJava; 

import org.openqa.selenium.By; 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import cucumber.api.java.en.Given; 
import cucumber.api.java.en.Then; 
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import java.util.List;
import org.junit.Assert;

public class stepBindings { 
	
   WebDriver driver = null; 
   String endPoint = "http://www.sportsdirect.com/";
   String webDriver = "webdriver.gecko.driver";
   String webDriverPath = "C:\\temp\\geckodriver-v0.18.0-win32\\geckodriver.exe";
   String email = "atis.test@inbox.lv";
   int wait = 4000;
	
   
   @Given("^I open web browser$") 
   public void openBrowser() { 
	  
	  System.setProperty(webDriver,webDriverPath);
      driver = new FirefoxDriver();
   } 
	
   
   @When("^I open Sportdirect website$") 
   public void openWeb() { 
	   
      driver.navigate().to(endPoint);          
   } 
   
   
   @And("^I close popup dialog$")
   public void closeDialog() throws InterruptedException{
	   	   		  	  	
	  driver.findElement(By.cssSelector("input[id='inputAcceptCookies'][value='X']")).click();
	  Thread.sleep(wait);
	  driver.findElement(By.cssSelector("input[type='button']")).click();
   }
  
   
   @And("^I open Mens menu$")
   public void openMenu() throws InterruptedException {
	   
	   driver.findElement(By.xpath("//a[contains(text(), 'Mens')]")).click();	  
   }
   
   
   @And("^I click on Mens shoes link$")
   public void openSubMenu() throws InterruptedException {
	  
	  driver.findElement(By.cssSelector("a[href*='mens-shoes'")).click();
	  Thread.sleep(wait);
	  closeDialog();   	  
   }
     
   
   @And("^I select Firetrap and Skechers brands$")
   public void selectItems() throws InterruptedException {
	   Thread.sleep(wait);
	   driver.findElement(By.xpath("//span[contains(text(), 'Firetrap')]")).click();
	   driver.findElement(By.xpath("//span[contains(text(), 'Skechers')]")).click();
   }
   
   
   @And("^I select the prices between 30 and 60 EUR's$")
   public void selectPrice() throws InterruptedException {
	   
	   Thread.sleep(wait);
	   WebElement entryMin=driver.findElement(By.id("PriceFilterTextEntryMin"));
	   WebElement entryMax=driver.findElement(By.id("PriceFilterTextEntryMax"));
	   entryMin.sendKeys("30");
	   entryMax.sendKeys("60");
   }
   
   
   @And("^I press button$")
   public void pressButton() {
	  	   
	   driver.findElement(By.cssSelector("input[id='PriceFilterTextEntryApply'][value='Go']")).click();
   }
   
   
   @Then("^I validate that I've selected only Firetrap and Skechers brands$")
   public void sortItem() throws InterruptedException {
	  	   
	   Select dropdown = new Select(driver.findElement(By.cssSelector("[id$=_ViewTemplate_ctl00_sortOptions1_ddlSortOptions]")));
	   dropdown.selectByVisibleText("Price (Low To High)");
	   
	   Thread.sleep(wait);
	   
	   Boolean isBrandListed=true;
	   List<WebElement> elements = driver.findElements(By.className("productdescriptionbrand"));
	   for (WebElement element: elements) {
		      if(!element.getText().equals("Firetrap") && !element.getText().equals("Skechers"))
		      {		    	  
		    	  isBrandListed=false;
		    	  break;		    	      	  
		      }	     
	   }
	   
	   Assert.assertEquals(true, isBrandListed);
   }
   
      
   @And("^I validate that prices are between 30 and 60 EUR's$")
   public void sortPrice() throws InterruptedException {
	  	   
	   List<WebElement> priceListMin = driver.findElements(By.className("CurrencySizeLarge"));
	   String minValue = priceListMin.get(0).getText().trim();
	   	   
	   Select dropdown = new Select(driver.findElement(By.cssSelector("[id$=_ViewTemplate_ctl00_sortOptions1_ddlSortOptions]")));
	   dropdown.selectByVisibleText("Price (High To Low)");
	   
	   Thread.sleep(wait);

	   List<WebElement> priceListMax = driver.findElements(By.className("CurrencySizeLarge"));
	   
	   String maxValue = priceListMax.get(0).getText().trim();
	   
	   String expectedMin = "30,00 �";
	   String expectedMax = "60,00 �";
	   
	   Boolean isPricesCorrect = false;
	   	   
	   if(minValue.equals(expectedMin) && maxValue.equals(expectedMax))
	   {
		   isPricesCorrect = true;
	   }
	   
	   Assert.assertEquals(true, isPricesCorrect);	   
   }

   
   @And("^Browser is closed$")
   public void closeBrowser() {
	   
	   driver.close();
   }
   
   
   @And("^I click SignIn button$")
   public void signIn() throws InterruptedException {
	   
	   driver.findElement(By.cssSelector("[id$=LOGIN_loginLink]")).click();
	   Thread.sleep(wait);	   
   }
   
   
   @And("^I click on email recovery link$")
   public void recoveryOpen() throws InterruptedException {
	   
	   driver.findElement(By.cssSelector("[id$=_LoginScreen_registerLogin_cmdForgottenPassword]")).click();
	   Thread.sleep(wait);	   
   }
   
   
   @And("^I fill in e-mail address$")
   public void fillEmailAddress() {
	   
	   WebElement emailAddress = driver.findElement(By.cssSelector("[id$=_PasswordReset_UserName]"));
	   emailAddress.sendKeys(email);	   
   }
   
   
   @And("^I press SendEmail button$")
   public void sendEmail() throws InterruptedException {
	   
	   driver.findElement(By.cssSelector("[id$=_PasswordReset_cmdSendPassword]")).click();
	   Thread.sleep(wait);	   
   }
   
   
   @Then("^Recovery confirmation message appears$")
   public void recoveryConfirmation() {
	   
	   boolean isPasswordRecovered = driver.findElement(By.cssSelector("[id$=_ctl00_lblMessage]")).isDisplayed();
	   
	   Assert.assertEquals(true, isPasswordRecovered);	   
   }
}
   