Feature: SearchFiltering

Scenario: Item filter functionality works 

	Given I open web browser

	When I open Sportdirect website

		And I close popup dialog

		And I open Mens menu

		And I click on Mens shoes link

		And I select Firetrap and Skechers brands

		And I select the prices between 30 and 60 EUR's

		And I press button

	Then  I validate that I've selected only Firetrap and Skechers brands

		And I validate that prices are between 30 and 60 EUR's

		And Browser is closed